import './module/sw-cms/blocks/text/benefit-element';
import './module/sw-cms/blocks/text/icon-text-element';
import './module/sw-cms/blocks/text-image/icon-content-element';
import './module/sw-cms/blocks/text-image/image-slider-element';
import './module/sw-cms/blocks/text-image/referenz-slider-element';
import './module/sw-cms/blocks/text-image/banner-element';
import './module/sw-cms/elements/benefit-element';
import './module/sw-cms/elements/icon-text-element';
import './module/sw-cms/elements/icon-content-element';
import './module/sw-cms/elements/image-slider-element';
import './module/sw-cms/elements/referenz-slider-element';
import './module/sw-cms/elements/banner-element';
import deDE from './module/sw-cms/snippet/de-DE.json';
import enGB from './module/sw-cms/snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
