import template from './sw-cms-el-preview-image-slider-element.html.twig';
import './sw-cms-el-preview-image-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-image-slider-element', {
    template
});
