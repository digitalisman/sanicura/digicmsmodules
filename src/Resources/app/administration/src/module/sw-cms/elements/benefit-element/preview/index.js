import template from './sw-cms-el-preview-benefit-element.html.twig';
import './sw-cms-el-preview-benefit-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-benefit-element', {
    template
});
