import template from './sw-cms-el-preview-referenz-slider-element.html.twig';
import './sw-cms-el-preview-referenz-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-referenz-slider-element', {
    template
});
