import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
	name: 'banner-element',
	label: 'Banner Element',
	component: 'sw-cms-el-banner-element',
	configComponent: 'sw-cms-el-config-banner-element',
	previewComponent: 'sw-cms-el-preview-banner-element',
	defaultConfig: {
		elementText: {
			source: 'static',
			value: null
		},
		elementButtonText: {
			source: 'static',
			value: null
		},
		elementButtonLink: {
			source: 'static',
			value: null
		},
		sliderItems: {
			source: 'static',
			value: [],
			required: true,
			entity: {
				name: 'media'
			}
		}
	},
	enrich: function enrich(elem, data) {
        if (Object.keys(data).length < 1) {
            return;
        }

        Object.keys(elem.config).forEach((configKey) => {
            const entity = elem.config[configKey].entity;

            if (!entity) {
                return;
            }

            const entityKey = entity.name;
            if (!data[`entity-${entityKey}`]) {
                return;
            }

            elem.data[configKey] = [];
            elem.config[configKey].value.forEach((sliderItem) => {
                elem.data[configKey].push({
                    newTab: sliderItem.newTab,
                    url: sliderItem.url,
                    media: data[`entity-${entityKey}`].get(sliderItem.mediaId)
                });
            });
        });
    }
});
