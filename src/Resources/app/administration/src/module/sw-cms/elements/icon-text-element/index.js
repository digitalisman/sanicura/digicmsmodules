import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
	name: 'icon-text-element',
	label: 'Icon Text Element',
	component: 'sw-cms-el-icon-text-element',
	configComponent: 'sw-cms-el-config-icon-text-element',
	previewComponent: 'sw-cms-el-preview-icon-text-element',
	defaultConfig: {
		elementIcon: {
			source: 'static',
			value: null
		},
		elementText: {
			source: 'static',
			value: null
		},
		elementSubtext: {
			source: 'static',
			value: null
		},
		elementValue: {
			source: 'static',
			value: '99'
		},
		/*
		elementVisuals: {
			source: 'static',
			value: null,
			required: false,
			entity: {
				name: 'media'
			}
		},*/
		sliderItems: {
			source: 'static',
			value: [],
			required: true,
			entity: {
				name: 'media'
			}
		}
	},
	enrich: function enrich(elem, data) {
        if (Object.keys(data).length < 1) {
            return;
        }

        Object.keys(elem.config).forEach((configKey) => {
            const entity = elem.config[configKey].entity;

            if (!entity) {
                return;
            }

            const entityKey = entity.name;
            if (!data[`entity-${entityKey}`]) {
                return;
            }

            elem.data[configKey] = [];
            elem.config[configKey].value.forEach((sliderItem) => {
                elem.data[configKey].push({
                    newTab: sliderItem.newTab,
                    url: sliderItem.url,
                    media: data[`entity-${entityKey}`].get(sliderItem.mediaId)
                });
            });
        });
    }
});
