import template from './sw-cms-el-config-benefit-element.html.twig';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-benefit-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('benefit-element');
        }
    }
});
