import template from './sw-cms-el-preview-banner-element.html.twig';
import './sw-cms-el-preview-banner-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-banner-element', {
    template
});
