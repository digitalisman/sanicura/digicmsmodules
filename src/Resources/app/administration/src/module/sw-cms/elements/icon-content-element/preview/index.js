import template from './sw-cms-el-preview-icon-content-element.html.twig';
import './sw-cms-el-preview-icon-content-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-icon-content-element', {
    template
});
