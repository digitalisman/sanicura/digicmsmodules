import template from './sw-cms-el-preview-icon-text-element.html.twig';
import './sw-cms-el-preview-icon-text-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-icon-text-element', {
    template
});
