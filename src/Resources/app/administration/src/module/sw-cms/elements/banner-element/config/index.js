import template from './sw-cms-el-config-banner-element.html.twig';

const { Component, Mixin } = Shopware;
const { cloneDeep } = Shopware.Utils.object;
const Criteria = Shopware.Data.Criteria;

Component.register('sw-cms-el-config-banner-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],
    
    inject: ['repositoryFactory'],
    
    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
            enitiy: this.element,
            mediaItems: []
        };
    },
    
    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        defaultFolderName() {
            return this.cmsPageState._entityName;
        },

        sliderItems() {
            if (this.element.data && this.element.data.sliderItems && this.element.data.sliderItems.length > 0) {
                return this.element.data.sliderItems;
            }

            return [];
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
	    async createdComponent() {
			this.initElementConfig('banner-element');
			if (this.element.config.sliderItems.value.length > 0) {
                const mediaIds = this.element.config.sliderItems.value.map((configElement) => {
                    return configElement.mediaId;
                });

                const criteria = new Criteria();
                criteria.setIds(mediaIds);

                const searchResult = await this.mediaRepository.search(criteria, Shopware.Context.api);
                this.mediaItems = mediaIds.map((mediaId) => {
                    return searchResult.get(mediaId);
                });
            }
	    },
	    onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },
        onCloseMediaModal() {
            this.mediaModalIsOpen = false;
        },
        onImageUpload(mediaItem) {
            this.element.config.sliderItems.value.push({
                mediaUrl: mediaItem.url,
                mediaId: mediaItem.id,
                url: null,
                newTab: false
            });

            this.mediaItems.push(mediaItem);
            this.updateMediaDataValue();
            this.emitUpdateEl();
        },
        onItemRemove(mediaItem, index) {
            const key = mediaItem.id;
            this.element.config.sliderItems.value =
                this.element.config.sliderItems.value.filter(
                    (item, i) => (item.mediaId !== key || i !== index)
                );

            this.mediaItems = this.mediaItems.filter(
                (item, i) => (item.id !== key || i !== index)
            );

            this.updateMediaDataValue();
            this.emitUpdateEl();
        },
        onMediaSelectionChange(mediaItems) {
            mediaItems.forEach((item) => {
                this.element.config.sliderItems.value.push({
                    mediaUrl: item.url,
                    mediaId: item.id,
                    url: null,
                    newTab: false,
                    contentTitle: false,
                    contentText: false,
                    buttonText: false
                });
            });

            this.mediaItems.push(...mediaItems);
            this.updateMediaDataValue();
            this.emitUpdateEl();
        },
        updateMediaDataValue() {
            if (this.element.config.sliderItems.value) {
                const sliderItems = cloneDeep(this.element.config.sliderItems.value);

                sliderItems.forEach((galleryItem) => {
                    this.mediaItems.forEach((mediaItem) => {
                        if (galleryItem.mediaId === mediaItem.id) {
                            galleryItem.media = mediaItem;
                        }
                    });
                });
                this.$set(this.element.data, 'sliderItems', sliderItems);
            }
        },
        emitUpdateEl() {
            this.$emit('element-update', this.element);
        }
    }
});
