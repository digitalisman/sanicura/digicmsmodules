import template from './sw-cms-el-icon-content-element.html.twig';
import './sw-cms-el-icon-content-element.scss';

const { Component, Mixin, Filter } = Shopware;

Component.register('sw-cms-el-icon-content-element', {
	template,

	mixins: [
		Mixin.getByName('cms-element')
	],
	created() {
		this.createdComponent();
	},
	methods: {
		createdComponent() {
			this.initElementConfig('icon-content-element');
			this.initElementData('icon-content-element');
		}
	}
});
