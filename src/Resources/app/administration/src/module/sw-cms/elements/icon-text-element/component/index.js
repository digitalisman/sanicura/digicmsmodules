import template from './sw-cms-el-icon-text-element.html.twig';
import './sw-cms-el-icon-text-element.scss';

const { Component, Mixin, Filter } = Shopware;

Component.register('sw-cms-el-icon-text-element', {
	template,

	mixins: [
		Mixin.getByName('cms-element')
	],
	computed: {
		concatedText() {
			if (this.element.config.elementSubtext.value === '' || this.element.config.elementValue.value === '') {
				return null;
			}
			
			var newText = this.element.config.elementSubtext.value.replace("#",this.element.config.elementValue.value).replace("%","<span>%</span>");
			
			return newText;
		}
	},
	created() {
		this.createdComponent();
	},
	methods: {
		createdComponent() {
			this.initElementConfig('icon-text-element');
			this.initElementData('icon-text-element');
		}
	}
});
