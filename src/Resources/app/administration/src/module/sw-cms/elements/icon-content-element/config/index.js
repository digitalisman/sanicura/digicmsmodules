import template from './sw-cms-el-config-icon-content-element.html.twig';

const { Component, Mixin } = Shopware;
const { cloneDeep } = Shopware.Utils.object;
const Criteria = Shopware.Data.Criteria;

Component.register('sw-cms-el-config-icon-content-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],
    created() {
        this.createdComponent();
    }
});
