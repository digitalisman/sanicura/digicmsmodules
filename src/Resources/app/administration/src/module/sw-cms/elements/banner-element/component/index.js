import template from './sw-cms-el-banner-element.html.twig';
import './sw-cms-el-banner-element.scss';

const { Component, Mixin, Filter } = Shopware;

Component.register('sw-cms-el-banner-element', {
	template,

	mixins: [
		Mixin.getByName('cms-element')
	],
	data() {
		return {
			columnCount: 7,
			columnWidth: 90,
			sliderPos: 0,
			imgPath: '/administration/static/img/cms/preview_mountain_large.jpg',
			imgSrc: ''
		};
	},
	computed: {
		uploadTag() {
			return `cms-element-media-config-${this.element.id}`;
		},
		sliderItems() {
			if (this.element.data && this.element.data.sliderItems && this.element.data.sliderItems.length > 0) {
				return this.element.data.sliderItems;
			}
		
			return [];
		},
		contextAssetPath() {
			return Shopware.Context.api.assetsPath;
		}
	},
	watch: {
		'element.data.sliderItems': {
			handler() {
				if (this.sliderItems.length > 0) {
					this.imgSrc = this.sliderItems[0].media.url;
					this.$emit('active-image-change', this.sliderItems[0].media);
				} else {
					this.imgSrc = `${this.contextAssetPath}${this.imgPath}`;
				}
			},
			deep: true
		},
			
		activeMedia() {
			this.sliderPos = this.activeMedia.sliderIndex;
			this.imgSrc = this.activeMedia.url;
		}
	},
	created() {
		this.createdComponent();
	},
	methods: {
		createdComponent() {
			this.initElementConfig('banner-element');
			this.initElementData('banner-element');
			if (this.element.data && this.element.data.sliderItems && this.element.data.sliderItems.length > 0) {
				this.imgSrc = this.sliderItems[0].media.url;
				this.$emit('active-image-change', this.sliderItems[this.sliderPos].media);
			} else {
				this.imgSrc = `${this.contextAssetPath}${this.imgPath}`;
			}
		},
		setSliderItem(mediaItem, index) {
			this.imgSrc = mediaItem.url;
			this.$emit('active-image-change', mediaItem, index);
		},
	}
});
