import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
	name: 'benefit-element',
	label: 'Benefit Cols',
	component: 'sw-cms-el-benefit-element',
	configComponent: 'sw-cms-el-config-benefit-element',
	previewComponent: 'sw-cms-el-preview-benefit-element',
	defaultConfig: {
		benefitText: {
			source: 'static',
			value: null
		},
		benefitIcon: {
			source: 'static',
			value: null
		}
	}
});
