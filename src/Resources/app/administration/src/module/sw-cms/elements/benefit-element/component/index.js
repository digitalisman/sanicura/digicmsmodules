import template from './sw-cms-el-benefit-element.html.twig';
import './sw-cms-el-benefit-element.scss';

const { Component, Mixin, Filter } = Shopware;

Component.register('sw-cms-el-benefit-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('benefit-element');
            this.initElementData('benefit-element');
        }
    }
});
