import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
	name: 'icon-content-element',
	label: 'Icon Text Element',
	component: 'sw-cms-el-icon-content-element',
	configComponent: 'sw-cms-el-config-icon-content-element',
	previewComponent: 'sw-cms-el-preview-icon-content-element',
	defaultConfig: {
		elementIcon: {
			source: 'static',
			value: null
		},
		elementText: {
			source: 'static',
			value: null
		},
		elementSubtext: {
			source: 'static',
			value: null
		}
	}
});
