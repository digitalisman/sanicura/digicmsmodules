import template from './sw-cms-block-icon-content-element.html.twig';
import './sw-cms-block-icon-content-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-icon-content-element', {
    template
});
