import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'image-slider-element',
	label: 'Digitalisman Slider',
	category: 'text-image',
	component: 'sw-cms-block-image-slider-element',
	previewComponent: 'sw-cms-preview-image-slider-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'boxed'
	},
	slots: {
		one: 'image-slider-element'
	}
});
