import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'banner-element',
	label: 'Banner Element',
	category: 'text-image',
	component: 'sw-cms-block-banner-element',
	previewComponent: 'sw-cms-preview-banner-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'default'
	},
	slots: {
		one: 'banner-element'
	}
});
