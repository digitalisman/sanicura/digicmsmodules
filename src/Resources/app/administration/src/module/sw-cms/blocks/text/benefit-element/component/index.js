import template from './sw-cms-block-benefit-element.html.twig';
import './sw-cms-block-benefit-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-benefit-element', {
    template
});
