import template from './sw-cms-preview-banner-element.html.twig';
import './sw-cms-preview-banner-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-banner-element', {
    template
});
