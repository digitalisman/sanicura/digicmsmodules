import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'icon-text-element',
	label: 'Kunden Empfohlen',
	category: 'text',
	component: 'sw-cms-block-icon-text-element',
	previewComponent: 'sw-cms-preview-icon-text-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'boxed'
	},
	slots: {
		one: 'icon-text-element'
	}
});
