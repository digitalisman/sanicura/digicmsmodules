import template from './sw-cms-preview-benefit-element.html.twig';
import './sw-cms-preview-benefit-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-benefit-element', {
    template
});
