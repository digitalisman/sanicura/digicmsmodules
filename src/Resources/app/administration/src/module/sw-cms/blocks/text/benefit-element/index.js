import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'benefit-element',
	label: 'Benefit Element',
	category: 'text',
	component: 'sw-cms-block-benefit-element',
	previewComponent: 'sw-cms-preview-benefit-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'boxed'
	},
	slots: {
		one: 'benefit-element',
		two: 'benefit-element',
		three: 'benefit-element',
		four: 'benefit-element'
	}
});
