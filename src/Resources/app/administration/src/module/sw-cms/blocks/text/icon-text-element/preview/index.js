import template from './sw-cms-preview-icon-text-element.html.twig';
import './sw-cms-preview-icon-text-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-icon-text-element', {
    template
});
