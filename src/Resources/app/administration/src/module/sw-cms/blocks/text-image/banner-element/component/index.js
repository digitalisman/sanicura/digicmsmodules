import template from './sw-cms-block-banner-element.html.twig';
import './sw-cms-block-banner-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-banner-element', {
    template
});
