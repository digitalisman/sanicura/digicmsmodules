import template from './sw-cms-block-icon-text-element.html.twig';
import './sw-cms-block-icon-text-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-icon-text-element', {
    template
});
