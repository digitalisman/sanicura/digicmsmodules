import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'icon-content-element',
	label: '3 Cols Icon with Text',
	category: 'text-image',
	component: 'sw-cms-block-icon-content-element',
	previewComponent: 'sw-cms-preview-icon-content-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'boxed'
	},
	slots: {
		one: 'icon-content-element',
		two: 'icon-content-element',
		three: 'icon-content-element'
	}
});
