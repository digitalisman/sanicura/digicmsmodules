import template from './sw-cms-preview-icon-content-element.html.twig';
import './sw-cms-preview-icon-content-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-icon-content-element', {
    template
});
