import template from './sw-cms-preview-referenz-slider-element.html.twig';
import './sw-cms-preview-referenz-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-referenz-slider-element', {
    template
});
