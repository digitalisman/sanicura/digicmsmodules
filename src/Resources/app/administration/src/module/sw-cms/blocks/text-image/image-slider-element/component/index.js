import template from './sw-cms-block-image-slider-element.html.twig';
import './sw-cms-block-image-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-image-slider-element', {
    template
});
