import template from './sw-cms-preview-image-slider-element.html.twig';
import './sw-cms-preview-image-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-image-slider-element', {
    template
});
