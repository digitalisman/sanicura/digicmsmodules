import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
	name: 'referenz-slider-element',
	label: 'Kundenmeinungen',
	category: 'text-image',
	component: 'sw-cms-block-referenz-slider-element',
	previewComponent: 'sw-cms-preview-referenz-slider-element',
	defaultConfig: {
		marginBottom: '20px',
		marginTop: '20px',
		marginLeft: '20px',
		marginRight: '20px',
		sizingMode: 'boxed'
	},
	slots: {
		one: 'referenz-slider-element'
	}
});
