import template from './sw-cms-block-referenz-slider-element.html.twig';
import './sw-cms-block-referenz-slider-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-referenz-slider-element', {
    template
});
